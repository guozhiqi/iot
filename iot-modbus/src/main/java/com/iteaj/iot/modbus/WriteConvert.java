package com.iteaj.iot.modbus;

import com.iteaj.iot.format.DataFormat;
import com.iteaj.iot.format.DataFormatConvert;

/**
 * 用于对要写的值做数据转换
 */
public class WriteConvert {

    private byte[] write;

    protected WriteConvert(byte[] write) {
        this.write = write;
    }

    public static WriteConvert build(short value, DataFormat format) {
        return new WriteConvert(DataFormatConvert.getInstance(format).getBytes(value));
    }

    public static WriteConvert build(int value, DataFormat format) {
        return new WriteConvert(DataFormatConvert.getInstance(format).getBytes(value));
    }

    public static WriteConvert build(float value, DataFormat format) {
        return new WriteConvert(DataFormatConvert.getInstance(format).getBytes(value));
    }

    public static WriteConvert build(long value, DataFormat format) {
        return new WriteConvert(DataFormatConvert.getInstance(format).getBytes(value));
    }

    public static WriteConvert build(double value, DataFormat format) {
        return new WriteConvert(DataFormatConvert.getInstance(format).getBytes(value));
    }

    public byte[] getWrite() {
        return write;
    }
}
