package com.iteaj.iot.client.mqtt;

import cn.hutool.core.util.StrUtil;
import com.iteaj.iot.client.ClientConnectProperties;
import io.netty.handler.codec.mqtt.MqttConstant;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.netty.handler.codec.mqtt.MqttVersion;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * mqtt客户端连接配置
 */
public class MqttConnectProperties extends ClientConnectProperties {

    /**
     * clientId 用来标识唯一的连接
     */
    private String clientId;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;

    /**
     * 多长时间校验一次连接(秒)
     */
    private int keepAlive;

    /**
     * 遗嘱主题
     * 客户端断开后 将发送此主题
     */
    private String willTopic;
    /**
     * 遗嘱主题对应的消息
     */
    private String willMessage;
    /**
     * 遗嘱消息被发布时需要保留
     */
    private boolean willRetain;
    /**
     * cleanSession
     * 如果清理会话（CleanSession）标志被设置为0，服务端必须基于当前会话（使用客户端标识符识别）的状态恢复与客户端的通信。
     * 如果清理会话（CleanSession）标志被设置为1，客户端和服务端必须丢弃之前的任何会话并开始一个新的会话。会话仅持续和网络连接同样长的时间，即网络连接断开，会话结束。与这个会话关联的状态数据不能被任何之后的会话重用
     */
    private boolean cleanSession;
    /**
     * 发布遗嘱消息时使用的服务质量等级
     */
    private MqttQoS willQos = MqttQoS.AT_MOST_ONCE;
    /**
     * 版本(默认 3.1.1)
     */
    private MqttVersion version = MqttVersion.MQTT_3_1_1;

    /**
     * 报文的最大长度
     * @see MqttConstant#DEFAULT_MAX_BYTES_IN_MESSAGE
     * @see io.netty.handler.codec.mqtt.MqttDecoder#maxBytesInMessage
     * issue https://gitee.com/iteaj/iot/issues/I5NLVV
     */
    private int maxBytesInMessage = MqttConstant.DEFAULT_MAX_BYTES_IN_MESSAGE;

    /**
     * 以下两个字段使用的编码
     * @see #willMessage
     * @see #password
     */
    private Charset charset = StandardCharsets.UTF_8;

    /**
     * 使用本地和默认端口已经默认客户端id创建
     */
    public MqttConnectProperties(String clientId) {
        this("127.0.0.1", 1883, clientId);
    }

    /**
     * 使用本地和默认端口已经默认客户端id创建
     */
    public MqttConnectProperties(String remoteHost, String clientId) {
        this(remoteHost, 1883, clientId);
    }

    /**
     * 使用指定的客户端id创建
     * @param remoteHost
     * @param remotePort
     * @param clientId
     */
    public MqttConnectProperties(String remoteHost, Integer remotePort, String clientId) {
        this(remoteHost, remotePort, null, null, clientId);
    }

    public MqttConnectProperties(String remoteHost, Integer remotePort
            , String localHost, Integer localPort, String clientId) {
        super(remoteHost, remotePort, localHost, localPort, clientId);
        this.clientId = clientId;
        // keepAlive 默认30分钟检测一次
        this.keepAlive = 30 * 60;

        // 默认读60秒空闲发送Ping请求
        this.setReaderIdleTime(60);
        if(StrUtil.isBlank(this.clientId)) {
            throw new MqttClientException("clientId不能为空");
        }
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public byte[] getPasswordByCharset() {
        if(getPassword() != null) {
            return getPassword().getBytes(charset);
        } else {
            return null;
        }
    }

    public String getWillTopic() {
        return willTopic;
    }

    public void setWillTopic(String willTopic) {
        this.willTopic = willTopic;
    }

    public String getWillMessage() {
        return willMessage;
    }

    public byte[] getWillMessageByCharset() {
        if(getWillMessage() != null) {
            return getWillMessage().getBytes(charset);
        } else {
            return null;
        }
    }

    public void setWillMessage(String willMessage) {
        this.willMessage = willMessage;
    }

    public boolean isWillRetain() {
        return willRetain;
    }

    public void setWillRetain(boolean willRetain) {
        this.willRetain = willRetain;
    }

    public boolean isCleanSession() {
        return cleanSession;
    }

    public void setCleanSession(boolean cleanSession) {
        this.cleanSession = cleanSession;
    }

    public MqttQoS getWillQos() {
        return willQos;
    }

    public void setWillQos(MqttQoS willQos) {
        this.willQos = willQos;
    }

    public MqttVersion getVersion() {
        return version;
    }

    public void setVersion(MqttVersion version) {
        this.version = version;
    }

    public Charset getCharset() {
        return charset;
    }

    public void setCharset(Charset charset) {
        this.charset = charset;
    }

    public int getMaxBytesInMessage() {
        return maxBytesInMessage;
    }

    public void setMaxBytesInMessage(int maxBytesInMessage) {
        this.maxBytesInMessage = maxBytesInMessage;
    }

    public int getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(int keepAlive) {
        this.keepAlive = keepAlive;
    }
}
