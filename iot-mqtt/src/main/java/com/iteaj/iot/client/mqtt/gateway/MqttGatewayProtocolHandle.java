package com.iteaj.iot.client.mqtt.gateway;

import com.iteaj.iot.client.ClientProtocolHandle;

public interface MqttGatewayProtocolHandle extends ClientProtocolHandle<MqttGatewayProtocol> {

    @Override
    Object handle(MqttGatewayProtocol protocol);
}
