package com.iteaj.iot.test.listener;

import com.iteaj.iot.FrameworkComponent;
import com.iteaj.iot.event.AsyncStatusEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 服务端异步事件监听
 */
public class AsyncServerTestListener implements AsyncStatusEventListener {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void offline(String source, FrameworkComponent component) {
        logger.info("服务端异步事件监听({}) 掉线 - 客户端编号：{} - 线程：{} - 测试通过", component.getName(), source, Thread.currentThread().getName());
    }

    @Override
    public void online(String source, FrameworkComponent component) {
        logger.info("服务端异步事件监听({}) 上线 - 客户端编号：{} - 线程：{} - 测试通过", component.getName(), source, Thread.currentThread().getName());
    }

}
