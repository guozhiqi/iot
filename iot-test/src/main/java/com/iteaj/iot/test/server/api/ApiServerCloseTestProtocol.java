package com.iteaj.iot.test.server.api;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.server.protocol.ClientInitiativeProtocol;

public class ApiServerCloseTestProtocol extends ClientInitiativeProtocol<ApiTestServerMessage> {

    public ApiServerCloseTestProtocol(ApiTestServerMessage requestMessage) {
        super(requestMessage);
    }

    @Override
    protected ApiTestServerMessage doBuildResponseMessage() {
        String equipCode = getEquipCode();
        DefaultMessageHead messageHead = new DefaultMessageHead(equipCode, protocolType(), "Api:Close:Resp\n".getBytes());
        return new ApiTestServerMessage(messageHead);
    }

    @Override
    protected void doBuildRequestMessage(ApiTestServerMessage requestMessage) {

    }

    @Override
    public ProtocolType protocolType() {
        return ApiProtocolType.Close;
    }
}
