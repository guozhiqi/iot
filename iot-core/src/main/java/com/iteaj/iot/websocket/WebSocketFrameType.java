package com.iteaj.iot.websocket;

public enum WebSocketFrameType {
    Text, Binary, Close
}
