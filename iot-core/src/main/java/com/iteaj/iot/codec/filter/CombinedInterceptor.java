package com.iteaj.iot.codec.filter;

import com.iteaj.iot.FrameworkComponent;

/**
 * 联合客户端注册以及解码拦截
 * @param <C>
 */
public interface CombinedInterceptor<C extends FrameworkComponent> extends ClientRegister<C>, DecoderInterceptor<C> {

    CombinedInterceptor DEFAULT = new CombinedInterceptor(){};

}
