package com.iteaj.iot;

import io.netty.channel.ChannelFuture;

import java.util.Optional;

public interface SocketDeviceManager<T> extends DeviceManager<T> {

    /**
     * 写出报文
     * @param equipCode 设备编号
     * @param msg 发送的协议
     * @param args 自定义参数
     * @return
     */
    Optional<ChannelFuture> writeAndFlush(String equipCode, Object msg, Object... args);

    /**
     * 写出协议
     * @see Protocol#requestMessage() 请求的报文
     * @see Protocol#responseMessage() 响应的报文
     * @param equipCode 设备编号
     * @param protocol 要写出的协议
     * @return
     */
    Optional<ChannelFuture> writeAndFlush(String equipCode, Protocol protocol);
}
