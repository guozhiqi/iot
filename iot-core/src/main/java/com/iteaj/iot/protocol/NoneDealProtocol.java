package com.iteaj.iot.protocol;

import com.iteaj.iot.SocketMessage;
import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.server.ServerMessage;

/**
 * 此协议将不做任何处理
 */
public class NoneDealProtocol extends AbstractProtocol<SocketMessage> {

    protected NoneDealProtocol(SocketMessage message) {
        this.requestMessage = message;
    }

    public static NoneDealProtocol getInstance(SocketMessage message) {
        return new NoneDealProtocol(message);
    }


    @Override
    public CommonProtocolType protocolType() {
        return CommonProtocolType.NoneMap;
    }

    @Override
    public AbstractProtocol buildRequestMessage() {
        throw new UnsupportedOperationException("不支持此操作");
    }

    @Override
    public AbstractProtocol buildResponseMessage() {
        throw new UnsupportedOperationException("不支持此操作");
    }
}
